package com.company;

public class Main {
    public static class Passenger {
        private String name;
        private double weight;

        Passenger(String name, double weight) {
            this.name = name;
            this.weight = weight;
        }
    }

    public static void checkWeight(double weight) throws ExceedWeightException {
        if (weight > 15.0) {
            throw new ExceedWeightException("weight should be less than or equal to 15kg");
        } else {
            System.out.println("weight within limit");
        }

    }


    public static void main(String[] args) {
        Passenger passenger1 = new Passenger("Bhavya", 15.1);
        Passenger passenger2 = new Passenger("Sruthi", 18.0);

        try{
            checkWeight(passenger2.weight);
        } catch (ExceedWeightException ex) {
            System.out.println("Caught the exception");
            System.out.println("Exception : " + ex);
        }finally {
            if(passenger2.weight>15.0 && passenger2.weight<16.0){
                System.out.println("additional cost can be ignored");
            }
            if(passenger2.weight >= 16.0){
                System.out.println("additional cost is");
                System.out.println((passenger2.weight-15.0)*500);
            }
        }
    }
}
