package com.company;

class ExceedWeightException extends Exception {
    public ExceedWeightException(String errorMessage) {
        super(errorMessage);
    }
}
